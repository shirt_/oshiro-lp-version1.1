const gulp = require("gulp")
const webpackStream = require("webpack-stream")
const webpack = require("webpack");
const pug = require("gulp-pug")
var autoprefixer = require('gulp-autoprefixer')
const sass = require("gulp-sass")
var plumber = require("gulp-plumber")
var notify = require("gulp-notify")
const browserSync = require("browser-sync").create()
const webpackConfig = require("./webpack.config")
const packageImporter = require('node-sass-package-importer')

const { watch, series, task, src, dest } = require('gulp')

// sassをcssに変換してautoprefixer
task('sass', function(done) {
    browserSync.reload();
    return src(["./src/sass/**/*.scss","!./src/sass/**/_*.scss"])
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(sass({
            outputStyle: 'expanded',
            importer: packageImporter({
                extensions: ['.scss', '.css']
            })
        }))
        // .pipe(autoprefixer({}))
        .pipe(dest("./dist/assets/css"))
    done();
  });

// pugをhtmlに変換
task('pug', function(done) {
    browserSync.reload();
    var option = {
        pretty: true
    }
    return src(["./src/pug/**/*.pug","!./src/pug/**/_*.pug"])
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(pug(option))
        .pipe(dest("./dist/"))
    done();
});

//local server start
task("browserSync",function(done){
    browserSync.init({
        server: {
            baseDir: "./dist/"
        }
    });
    done();
})

//webpack
task("webpack",function(done){
    browserSync.reload();
    return webpackStream(webpackConfig, webpack).pipe(gulp.dest("./dist/assets/js"));
    done();
})

// sassとpugの監視をして変換処理させる
watch(['./src/sass/**'], task('sass'));
watch(['./src/pug/**'], task('pug'));
watch(['./src/js/**'], task('webpack'));

task('default', series("browserSync",'sass', 'pug'));
