export default class AboutPage {
	constructor() {
		this.mapInit();
	}

	mapInit() {
		const api = "AIzaSyDjkCSHn9Jb3jdfFNwxWGsZjWDt9XL8TVU";
		const MyLatLng = new google.maps.LatLng(35.691275, 139.787828);
		const Options = {
			zoom: 15,
			center: MyLatLng,
			mapTypeId: 'roadmap',
			disableDefaultUI: true,
			styles:
				[{ featureType: "all", elementType: "all", stylers: [{ visibility: "on" }] }, { featureType: "administrative", elementType: "all", stylers: [{ saturation: "-100" }] }, { featureType: "administrative.province", elementType: "all", stylers: [{ visibility: "off" }] }, { featureType: "administrative.locality", elementType: "all", stylers: [{ visibility: "on" }] }, { featureType: "administrative.neighborhood", elementType: "all", stylers: [{ visibility: "on" }] }, { featureType: "landscape", elementType: "all", stylers: [{ saturation: -100 }, { lightness: 65 }, { visibility: "simplified" }] }, { featureType: "landscape", elementType: "labels", stylers: [{ visibility: "on" }] }, { featureType: "landscape", elementType: "labels.text", stylers: [{ visibility: "off" }] }, { featureType: "poi", elementType: "all", stylers: [{ saturation: -100 }, { lightness: "50" }, { visibility: "off" }] }, { featureType: "poi", elementType: "labels.text", stylers: [{ visibility: "off" }] }, { featureType: "poi.attraction", elementType: "all", stylers: [{ visibility: "off" }] }, { featureType: "poi.business", elementType: "all", stylers: [{ visibility: "off" }] }, { featureType: "road", elementType: "all", stylers: [{ saturation: "-100" }] }, { featureType: "road.highway", elementType: "all", stylers: [{ visibility: "simplified" }] }, { featureType: "road.arterial", elementType: "all", stylers: [{ lightness: "30" }, { visibility: "on" }] }, { featureType: "road.local", elementType: "all", stylers: [{ lightness: "40" }] }, { featureType: "transit", elementType: "all", stylers: [{ saturation: -100 }, { visibility: "simplified" }] }, { featureType: "water", elementType: "geometry", stylers: [{ hue: "#ffff00" }, { lightness: -25 }, { saturation: -97 }] }, { featureType: "water", elementType: "labels", stylers: [{ lightness: -25 }, { saturation: -100 }] }]
		};
		const map = new google.maps.Map(document.getElementById('map'), Options);
		const image = {
			url: '/assets/img/about/icon-map.png',
			scaledSize: new google.maps.Size(88, 94)
		}
		const markerOptions = {
			map: map,
			position: MyLatLng,
			icon: "/assets/img/about/icon-map.png",
			icon: image
		};
		const marker = new google.maps.Marker(markerOptions);
	}



}