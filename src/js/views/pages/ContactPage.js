import $ from 'jquery';

export default class ContactPage {
	constructor() {
        this.fileTypeFunction()
        this.checkPolicy()
    }
    fileTypeFunction(){
        $("#file").on("change",function(){
            const file = $(this).prop('files')[0];
            $("#fileName").text(file.name)
        });
    }

    checkPolicy(){
        const btnSubmit = $("#btnSend")
        const btnInput = $("#check-policy")

        btnInput.on("change",function(){
            btnSubmit.toggleClass("inActive")
        })

    }
}