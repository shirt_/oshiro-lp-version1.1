import $ from 'jquery';
import TweenMax from 'gsap';
import Barba from 'barba.js';
import Rellax from "rellax";
import AboutPageJS from "./pages/AboutPage"
import ContactPageJS from "./pages/ContactPage"

export default class Common {
	constructor() {
		this.$el = {
			header: $("header"),
			humbergerBtn: $("#humbergerBtn"),
			humbergerContent: $("#humbergerContent"),
			humbergerNavi: $("#humbergerContent nav"),
			headerInner: $("header .section-inner"),
			sectionService: $("#sectionService"),
			headerList: $("header nav .navi-global li .main-navi"),
			naviTitle: $(".navi-title")
		}
		
		this.init();
		this.barbaSetting();
		this.headerNaviMenu();
		this.scrollView();
		this.smoothScroll();
		this.scrollHeaderStyleChange();
		this.resizeFn()
	}
	init() {
		const rellax = new Rellax('.rellax');
		//初回アクセスかどうか判定
		sessionStorage.setItem('firstAccess', 'true');
		if (sessionStorage.getItem("firstAccess") === "true") {
			$("main").addClass("firstAccess");
			sessionStorage.clear();
		} else {
			$("main").removeClass("firstAccess");
		}

		// 初回表示時animationさせる為header / main / line-area それぞれclass付与
		$("header,main,.line-area").addClass("active");
		//SPのハンバーガーメニューのFn
		this.$el.humbergerBtn.on("click", () => {
			if (this.$el.humbergerBtn.hasClass("active")) {
				this.humbergerMenuClose();
			} else {
				this.humbergerMenuOpen();
			}
		});
	}


	scrollHeaderStyleChange() {
		let flag = true;
		$(window).on("scroll",  ()=> {
			let $windowScrollPosition = $(window).scrollTop();

			if($windowScrollPosition > this.scrollPosition && flag === true){
				flag = false;
				this.$el.headerInner.addClass("changeColor");
				$(".menu-modal-area,.humberger-btn").addClass("changeColor");
			}
			if($windowScrollPosition < this.scrollPosition && flag === false){
				
				flag = true
				this.$el.headerInner.removeClass("changeColor");
				$(".menu-modal-area,.humberger-btn").removeClass("changeColor");
				
			}
		});
	}

	humbergerMenuOpen() {
		this.$el.humbergerBtn.addClass("active");
		TweenMax.to(this.$el.humbergerContent, 1, {
			visibility: "visible",
			x: 0,
			ease: Power4.easeOut,
			delay: .3
		})
		TweenMax.to(this.$el.humbergerNavi, 1, {
			opacity: 1,
			y: 0,
			ease: Power4.easeOut,
			delay: .7
		});
	}
	humbergerMenuClose() {
		this.$el.humbergerBtn.removeClass("active");
		TweenMax.to(this.$el.humbergerNavi, 1, {
			opacity: 0,
			y: "20px",
			ease: Power4.easeOut
		});
		TweenMax.to(this.$el.humbergerContent, 1, {
			visibility: "hidden",
			x: "101vw",
			ease: Power4.easeOut
		})

	}


	barbaSetting() {
		const self = this;
		Barba.Pjax.Dom.wrapperId = 'contents';
		Barba.Pjax.Dom.containerClass = 'container';
		//page遷移時だけ発火する
		const PageTransition = Barba.BaseTransition.extend({
			start: function () {
				Promise
					.all([this.newContainerLoading, this.moveOut()])
					.then(this.moveIn.bind(this));
			},
			// 遷移前の処理 初期化等
			moveOut: function () {
				$(window).off("scroll");
				$("#contents").off("scroll");
				$('a[href^="#"]').off("click");
				$("header,main,.line-area").removeClass("active");
				$("header").removeClass("index");
				$("header .section-inner.changeColor,.menu-modal-area").removeClass("changeColor");
				$("html").addClass("transition-leave");
			},
			// 遷移後の処理
			moveIn: function () {
				var _this = this;
				var $el = $(this.newContainer);

				const changeTransitionColor = setTimeout(function(){
					$("html").addClass("transition-change-color");
					clearTimeout(changeTransitionColor);
				},700);

				const changeTransitionVisiblity = setTimeout(function(){
					$("html").addClass("transition-change-visiblity");
					clearTimeout(changeTransitionVisiblity);
				},800);

				const endTransition = setTimeout(function(){
					window.scrollTo(0, 0);
					$("header,main,.line-area").addClass("active");
					$("html").removeClass();
					_this.done();
					clearTimeout(endTransition);
				},1200);

				const reStartFunction = () =>{
					self.humbergerMenuClose();
					self.scrollHeaderStyleChange();
					self.scrollView();
					self.smoothScroll();
					const rellax = new Rellax('.rellax');
				}
				reStartFunction();
			}
		});

		// indexページの処理 headerのnaviの色を変える為class付与
		const NamespaceIndex = Barba.BaseView.extend({
			namespace: 'index',
			onEnter: () => {
				// 読み込みを開始した時の処理
				this.$el.header.addClass("index");
			},
			onEnterCompleted: () => {
				// トランジションを完了した時の処理
				this.scrollPosition = $(window).height()+120;

			},
			onLeave: ()=> {
				// 次のページへのトランジションが始まった時の処理
			},
			onLeaveCompleted: function () {
				// このページのcontainerが削除された時の処理
			}
		});
		const NamespaceSingle = Barba.BaseView.extend({
			namespace: 'single',
			onEnterCompleted: () => {
				this.scrollPosition = $(".section-head").height();
			}
		});
		const NamespaceAbout = Barba.BaseView.extend({
			namespace: 'about',
			onEnterCompleted: () => {
				this.scrollPosition = $(".section-head").height();
				new AboutPageJS()
			}
		});
		const NamespaceContact = Barba.BaseView.extend({
			namespace: 'contact',
			onEnterCompleted: () => {
				this.scrollPosition = $(".section-head").height();
				new ContactPageJS()
			}
		});
		NamespaceIndex.init();
		NamespaceSingle.init();
		NamespaceAbout.init();
		NamespaceContact.init();

		/* アニメーションの関数を実行
		--------------------------------------------------*/
		Barba.Pjax.getTransition = function () {
			return PageTransition;
		};
		Barba.Pjax.start();

	}


	headerNaviMenu() {
		const $triggerMenu = $("header nav .navi-global li.menu");
		const $menuModalArea = $(".menu-modal-area");
		const $menuLink = $("header nav .navi-global li a");
		const $whiteBox = $("header .menu-modal-area .white-box");
		const $iconArrow = $("header nav .navi-global li.menu a");
		let $key;

		const openMenu = function () {
			$key = $(this).data("layer");
			$("header nav .navi-global li").removeClass("selected");
			$("header nav .navi-global li a").addClass("color-black");
			TweenMax.set(".menu-modal",{
				"display":"none",
				opacity:0,
				y:-20
			})
			TweenMax.set(`.menu-modal#${$key}`,{
				"display":"flex",
				opacity:0,
				y:-20
			})
			TweenMax.to(`.menu-modal#${$key}`,.3,{
				opacity:1,
				y:0,
				delay:.2,
				ease:Power4.easeOut
			})
			TweenMax.to($menuModalArea,.3,{
				visibility:"visible",
				opacity:1,
				ease:Power4.easeOut
			})

			
		}
		const closeMenu = () =>{
			$("header nav .navi-global li a").removeClass("color-black");
			TweenMax.set(".menu-modal",{
				"display":"none",
				opacity:0,
				y:-20
			})
			TweenMax.set($menuModalArea,{
				visibility:"hidden",
				opacity:0
			})
		}
		$triggerMenu.on("mouseenter", openMenu);
		$("header").on("mouseleave", closeMenu);
		$('header nav .navi-global li').not($triggerMenu).on("mouseenter",closeMenu);
	}

	smoothScroll() {
		$('a[href^="#"]').click(function () {
			var speed = 500;
			var href = $(this).attr("href");
			var target = $(href == "#" || href == "" ? 'html' : href);
			var position = target.offset().top;
			$("html, body").animate({ scrollTop: position }, speed, "swing");
			return false;
		});
	}

	scrollView() {
		const $target = $(".js-scroll");
		$(window).on("scroll", function () {
			$target.each(function () {
				let POS = $(this).offset().top;
				let scroll = $(window).scrollTop();
				let windowHeight = $(window).height();
				if (scroll > POS - windowHeight + windowHeight / 3.5) {
					$(this).addClass("isView");
				}
			});
		});
	}

	resizeFn(){
		let flag = false;
		const openAccordion =function() {
			$(this).next().stop().slideToggle(300);
			$(this).toggleClass("active");
		}
		$(window).on("load resize",()=>{
			if (resizeValue.getNowWindowWidth() <= 750) {
				if (flag == false) {
					this.$el.naviTitle.removeClass("active")
					this.$el.naviTitle.on("click",openAccordion);
					$(".list-navi").css({"display":"none"})
					flag = true;
				}
			} else {
				if (flag == true) {
					this.$el.naviTitle.removeClass("active")
					this.$el.naviTitle.off("click",openAccordion);
					$(".list-navi").css({"display":"flex"})
					flag = false;
				}
			}
		})

		const resizeValue = (()=>{
			let w, h;
			return {
				getNowWindowWidth:()=>{
					return w = $(window).width();
				},
				getNowWindowHeight:()=>{
					return h = $(window).height();
				}
			}
		})();
	}
}