var mode;
if (process.env.NODE_ENV === 'production') {
    mode = "production";
} else {
    mode = "development";
}
module.exports = {
    // モード値を production に設定すると最適化された状態で、
    // development に設定するとソースマップ有効でJSファイルが出力される
    mode: mode,
    watch: true,
    // メインのJS
    entry: "./src/js/main.js",
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ["css-loader"]
            },
            {
                test: /\.scss$/,
                use: [
                  'css-loader',
                  'sass-loader',
                ]
            },
            {
                // 拡張子 .js の場合
                test: /\.js$/,
                use: [
                    {
                        // Babel を利用する
                        loader: "babel-loader",
                        // Babel のオプションを指定する
                        options: {
                            presets: [
                                // プリセットを指定することで、ES2018 を ES5 に変換
                                "@babel/preset-env"
                            ]
                        }
                    }
                ]
            }
        ]
    },
    // 出力ファイル
    output: {
        filename: "bundle.js"
    }
}